#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include "haifile.h"
#include "hash_table.hpp"
#include "stack.h"
#include <cmath>
#include <stdint.h>

class StackProcessor {
private:
    void sDump(int line, const char *func) {
        st.Dump(__FILE__, line, func);
        printf("Regisers:\n");
        for (int i = 0; i < reg_count; i++) {
            printf("r%d:  can1=%d, can2=%d, value=%d\n", i, registers[i].canary1, registers[i].canary2, registers[i].value);
        }
        exit(1);
    }
    union Word {
        int integer;
        float fraction;
    };
    struct MemoryController {
        struct MemBlock { // ???
            int size;
            bool free;
            MemBlock *next;
        };
        MemBlock *start;
        void split(MemBlock *fitting_slot, int size) {
            MemBlock *new_block = (MemBlock*)((char*)fitting_slot + size + sizeof(MemBlock));
            new_block->size = (fitting_slot->size) - size - sizeof(MemBlock);
            new_block->free = true;
            new_block->next = fitting_slot->next;
            fitting_slot->size = size;
            fitting_slot->free = 0;
            fitting_slot->next = new_block;
        }
        static const int mib_size =  4;
        char *data;
        MemoryController() {
            data = (char *)calloc(mib_size * 1024 * 1024 + 8, 1);
            if (!data)
                assert(!"Initial memory allocation failed!");
            *(int *)(data) = OK_CANARY;
            *(int *)(data + 4 + mib_size * 1024 * 1024) = OK_CANARY;
            start = (MemBlock *)(data + 4);
            start->size = mib_size * 1024 * 1024 - sizeof(MemBlock);
            start->next = NULL;
            start->free = true;
        }
        int alloc_try(int size) {
            MemBlock *cur;
            cur = start;
            while ((cur->size < size || !cur->free) && cur->next != NULL) {
                cur = cur->next;
            }
            if (cur->free && cur->size == size) {
                cur->free = false;
                cur++;
                return (char *)cur - data;
            }
            if (cur->free && cur->size > size) {
                if (cur->size > size + sizeof(MemBlock)) {
                    split(cur, size);
                }
                cur->free = false;
                cur++;
                return (char *)cur - data;
            }
            return -1;
        }
        int allocate(int size, int *error_code) {
            if (!OK()) {*error_code = 1; return NULL;}
            int addr = alloc_try(size);
            if (addr != -1)
                return addr;
            merge();
            addr = alloc_try(size);
            if (addr != -1)
                return addr;
            assert(!"Memory allocation failed: not enough space");
            return NULL;
        }
        void merge() {
            MemBlock *cur = start;
            while (cur->next != NULL) {
                if (cur->free && cur->next->free) {
                    cur->size += cur->next->size;
                    cur->next = cur->next->next;
                }
                cur = cur->next;
            }
        }
        int free_memory(int addr) {
            if (!OK()) return 1;
            if (addr < mib_size * 1024 * 1024) {
                MemBlock *cur = (MemBlock *)(data + addr);
                cur--;
                cur->free = true;
                merge();
            } else {
                assert(!"Invalid free pointer");
                return 2;
            }
        }
        bool OK() {
            return this && *(int *)(data) == OK_CANARY &&  *(int *)(data + 4 + mib_size * 1024 * 1024) == OK_CANARY;
        }

    };
    struct Register {
        int canary1;
        int value;
        int canary2;
        bool OK() {
            return canary1 == OK_CANARY && canary2 == OK_CANARY;
        }
        int set_value(const int val) {
            if (!this) {assert(!"Register is null"); return 1;}
            if (!OK()) return 2;
            value = val;
            return 0;
        }
        int get_value(int *error_code = nullptr) {
            if (!this) {assert(!"Register is null"); if (error_code) *error_code = 1; return 0;}
            if (!OK()) {
                if (error_code)
                    *error_code = 2;
                return 0;
            }
            return value;
        }
        Register():
            canary1(OK_CANARY),
            value(0),
            canary2(OK_CANARY) {}
    };
    static const int reg_count = 10;
    Register registers[reg_count];
    SafeStack<int, 8> st;
    SafeStack<int, 8> call_stack;
    MemoryController memory;
    int programCounter;
    void (StackProcessor::*handlers[34])();
    char *code;
    int read_mem(int addr) {
        return *(int *)(memory.data + addr);
    }
    void write_mem(int addr, int arg) {
        *(int *)(memory.data + addr) = arg;
    }
    int mem_alloc(int size) {
        int error_code = 0;
        int addr = memory.allocate(size, &error_code);
        if (addr == NULL || error_code)
            sDump(__LINE__, __PRETTY_FUNCTION__);
        return addr;
    }
    void mem_free(int addr) {
        int error_code = memory.free_memory(addr);
        if (error_code)
            sDump(__LINE__, __PRETTY_FUNCTION__);
    }
    void end_handler() {
        exit(0);
    }
    int get_int() {
        int arg = *(int *)(code + programCounter);
        programCounter += 4;
        return arg;
    }
    void push_handler() {
        int arg = get_int();
        st.push(arg);
    }
    void pushm_handler() {
        int addr = get_int();
        int arg =  read_mem(addr);
        st.push(arg);
    }
    void popm_handler() {
        int addr = get_int();
        int arg = st.pop();
        write_mem(addr, arg);
    }
    void pushr_handler() {
        int reg_num = get_int();
        int error_code = 0;
        st.push(registers[reg_num].get_value(&error_code));
        if (error_code) {
            sDump(__LINE__, __PRETTY_FUNCTION__);
        }
    }
    void pop_handler() {
        int reg_num = get_int();
        int error_code = registers[reg_num].set_value(st.top());
        if (error_code) {
            sDump(__LINE__, __PRETTY_FUNCTION__);
        }
        st.pop();
    }
    void in_handler() {
        int value = 0;
        scanf("%d", &value);
        st.push(value);
    }
    void fin_handler() {
        Word value = {0};
        scanf("%f", &value.fraction);
        st.push(value.integer);
    }
    void add_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        st.push(arg1 + arg2);
    }
    void mul_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        st.push(arg1 * arg2);
    }
    void sub_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        st.push(arg2 - arg1);
    }
    void out_handler() {
        printf("%d\n", st.top());
        st.pop();
    }
    void fout_handler() {
        Word arg;
        arg.integer = st.top();
        printf("%f\n", arg.fraction);
    }
    void ja_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        int addr = get_int();
        if (arg2 > arg1)
            programCounter = addr;
    }
    void jae_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        int addr = get_int();
        if (arg2 >= arg1)
            programCounter = addr;
    }
    void jb_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        int addr = get_int();
        if (arg2 < arg1)
            programCounter = addr;
    }
    void jbe_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        int addr = get_int();
        if (arg2 <= arg1)
            programCounter = addr;
    }
    void je_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        int addr = get_int();
        if (arg2 == arg1)
            programCounter = addr;
    }
    void jne_handler() {
        int arg1 = st.top();
        st.pop();
        int arg2 = st.top();
        st.pop();
        int addr = get_int();
        if (arg2 != arg1)
            programCounter = addr;
    }
    void call_handler() {
        int addr = get_int();
        call_stack.push(programCounter);
        programCounter = addr;
    }
    void ret_handler() {
        int addr = call_stack.top();
        call_stack.pop();
        programCounter = addr;
    }
    void mal_handler() {
        int size = st.top();
        st.pop();
        int mem_addr = mem_alloc(size);
        st.push(mem_addr);
    }
    void free_handler() {
        int addr = st.top();
        st.pop();
        mem_free(addr);
    }
    void jmp_handler() {
        int addr = get_int();
        programCounter = addr;
    }
    void sqrt_handler() {
        Word a = {0};
        a.integer = st.top();
        st.pop();
        Word b = {0};
        b.fraction = sqrt(a.fraction);
        st.push(b.integer);
    }
    void fdiv_handler() {
        Word a1 = {0}, a2 = {0};
        a1.integer = st.top();
        st.pop();
        a2.integer = st.top();
        st.pop();
        Word b = {0};
        b.fraction = a2.fraction / a1.fraction;
        st.push(b.integer);
    }
    void itof_handler() {
        Word a = {0};
        float num = st.top();
        a.fraction = num;
        st.pop();
        st.push(a.integer);
    }
    void ftoi_handler() {
        Word a = {0};
        a.integer = st.top();
        int num = a.fraction;
        st.push(num);
    }
    void fmul_handler() {
        Word a = {0}, b = {0};
        a.integer = st.top();
        st.pop();
        b.integer = st.top();
        st.pop();
        Word res = {0};
        res.fraction = a.fraction * b.fraction;
        st.push(res.integer);
    }
    void fsub_handler() {
        Word a = {0}, b = {0};
        a.integer = st.top();
        st.pop();
        b.integer = st.top();
        st.pop();
        Word res = {0};
        res.fraction = b.fraction - a.fraction;
        st.push(res.integer);
    }
    void fadd_handler() {
        Word a = {0}, b = {0};
        a.integer = st.top();
        st.pop();
        b.integer = st.top();
        st.pop();
        Word res = {0};
        res.fraction = a.fraction + b.fraction;
        st.push(res.integer);
    }
public:
    StackProcessor() :
        programCounter(0),
        memory() {
        void (StackProcessor::*_handlers[34])() = {
                &StackProcessor::end_handler,
                &StackProcessor::push_handler,
                &StackProcessor::pushm_handler,
                &StackProcessor::popm_handler,
                &StackProcessor::pushr_handler,
                &StackProcessor::pop_handler,
                &StackProcessor::in_handler,
                &StackProcessor::fin_handler,
                &StackProcessor::add_handler,
                &StackProcessor::mul_handler,
                &StackProcessor::sub_handler,
                &StackProcessor::out_handler,
                &StackProcessor::fout_handler,
                &StackProcessor::jmp_handler,
                &StackProcessor::ja_handler,
                &StackProcessor::jae_handler,
                &StackProcessor::jb_handler,
                &StackProcessor::jbe_handler,
                &StackProcessor::je_handler,
                &StackProcessor::jne_handler,
                &StackProcessor::call_handler,
                &StackProcessor::ret_handler,
                &StackProcessor::mal_handler,
                &StackProcessor::free_handler,
                &StackProcessor::sqrt_handler,
                &StackProcessor::fdiv_handler,
                &StackProcessor::sqrt_handler,
                &StackProcessor::itof_handler,
                &StackProcessor::ftoi_handler,
                &StackProcessor::fout_handler,
                &StackProcessor::fin_handler,
                &StackProcessor::fmul_handler,
                &StackProcessor::fsub_handler,
                &StackProcessor::fadd_handler,
        };
        for (int i = 0; i < 34; i++) {
            handlers[i] = _handlers[i];
        }
    }
    void command_process() {
        int num = code[programCounter];
        programCounter++;
        void (StackProcessor::* hndl) () = handlers[num];
        (this->*hndl)();
    }
    void load_code(char *text, int size) {
        code = (char *)calloc(size, 1);
        memcpy(code, text, size);
    }
};

int main(int argc, char **argv) {
    StackProcessor processor;
    if (argc == 1) {
        printf("Usage: %s <filename>", argv[0]);
        exit(0);
    }
    int size = 0;
    char *buffer = read_file(argv[1], &size);
    processor.load_code(buffer, size);
    while (true) {
        processor.command_process();
    }
}