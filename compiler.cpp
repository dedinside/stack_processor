#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include "haifile.h"
#include "hash_table.hpp"
#define INT_OUT
const char default_keywords[] = "keywords";
char default_out_file[] = "a.out";
char default_lst_file[] = "a.lst";

class Compiler {
private:
    struct Command {
        char code;
        int argc;
        bool is_label;
    };
    char* filename;

    HashTable<int, Command, IntDoubleHashFunc> lexems;
    HashTable<light_string, int, LightStringDoubleHashFunc> labels;



    void read_source() {
        int file_size = 0;
        char *buffer = read_file(filename, &file_size);
        lines = divide_text(buffer, file_size, &lines_count);
    }

    void load_mapping(const char* keywords_filename) {
        int sz = 0;
        int cnt = 0;
        char *buf = read_file(keywords_filename, &sz);
        light_string* lex = divide_text(buf, sz, &cnt);
        for (int i = 0; i < cnt; i++) {
            int tmp = 0, j = 0;
            for (; j < lex[i].length && !isspace(lex[i][j]); j++)
                tmp = tmp * 26 + lex[i][j] - 'a';
            if (j == 0)
                continue;
            Command cmd = {i, lex[i][j + 1] - '0', lex[i][j + 3] - '0'};
            lexems.insert(tmp, cmd);
        }
    }

public:
    int *offset;
    int lines_count;
    light_string *lines;
    Compiler(const char *file):
            lines(nullptr),
            lines_count(0) {
        filename = strdup(file);
        load_mapping(default_keywords);
    }
    void add_to_buffer(bool flag, char **buffer, int *size, int *pos, void *data, int data_size) {
        if (flag) {
            while (*pos + data_size > (*size)) {
                *size *= 2;
                *buffer = (char *) realloc(*buffer, *size);
            }
            memcpy(*buffer + *pos, data, data_size);
        }
        *pos += data_size;
    }
    char* compile(int *size) {
        read_source();

        offset = (int *)calloc(lines_count, sizeof(4));
        char *text = compile_pass(false, size);
        free(text);
        text = compile_pass(true, size);
        return text;
    }
    char* compile_pass(bool is_write, int *_sz) {
        char *text = (char *)calloc(8, sizeof(char));
        int size = 8;
        int current_position = 0;
        for (int i = 0; i < lines_count; i++) {
            offset[i] = current_position;
            int it = 0;
            while (it < lines[i].length && isspace(lines[i][it]))
                it++;
            if (it == lines[i].length)
                continue;
            int _h = 0;
            int strt = it;
            while (it < lines[i].length && !isspace(lines[i][it])) {
                _h = _h * 26 + lines[i][it] - 'a';
                it++;
            }
            if (lines[i][it - 1] == ':') {
                if (!is_write) {
                    light_string label = lines[i];
                    label.ptr += strt;
                    label.length = it - strt - 1;
                    labels.insert(label, current_position);
                }
            } else {
                pair<bool, Command> res = lexems.find(_h);
                Command cmd = res.second;
                if (res.first) {
                    add_to_buffer(is_write, &text, &size, &current_position, &cmd.code, 1);
                } else {
                    printf("Syntax error at line %d : %s", i, lines[i].ptr);
                    exit(1);
                }
                if (cmd.argc) {
                    while (it < lines[i].length && isspace(lines[i][it]))
                        it++;
                    if (it == lines[i].length) {
                        printf("Argument expected at line %d : %s\n", i, lines[i].ptr);
                        exit(1);
                    }
                    if (cmd.is_label) {
                        light_string label = lines[i];
                        int label_name = 0;
                        while (it + label_name < label.length && !isspace(label[it + label_name]))
                            label_name++;
                        label.ptr += it;
                        label.length = label_name;
                        pair<bool, int> _res = labels.find(label);
                        if (_res.first || !is_write) {
                            add_to_buffer(is_write, &text, &size, &current_position, &_res.second, 4);
                        } else {
                            printf("Invalid label name at line %d : %s\n", i, lines[i].ptr);
                            exit(1);
                        }
                        it += label_name;
                    } else
                    if (isdigit(lines[i][it]) || lines[i][it] == '-') {
                        int num = 0, sym = 0;
                        if (sscanf(lines[i].ptr + it, "%d%n", &num, &sym) == -1) {
                            printf("Invalid number at line %d : %s\n", i, lines[i].ptr);
                            exit(1);
                        }
                        add_to_buffer(is_write, &text, &size, &current_position, &num, 4);
                        it += sym;
                    } else if (lines[i][it] == 'r') {
                        it++;
                        int num = 0, sym = 0;
                        if (sscanf(lines[i].ptr + it, "%d%n", &num, &sym) == -1) {
                            printf("Invalid register name at line %d : %s\n", i, lines[i].ptr);
                        }
                        add_to_buffer(is_write, &text, &size, &current_position, &num, 4);
                        it += sym;
                    } else {
                        printf("Invalid parameter at line %d : %s\n", i, lines[i].ptr);
                        exit(1);
                    }
                }
                while (it < lines[i].length && isspace(lines[i][it]))
                    it++;
                if (it != lines[i].length) {
                    printf("Syntax error at line %d : %s\n", i, lines[i].ptr);
                    exit(1);
                }
            }
        }
        *_sz = current_position;
        return text;
    }
};

int main(int argc, char **argv) {
    if (argc == 1) {
        printf("Usage: %s <OPTIONS> <FILE>\n", argv[0]);
        exit(1);
    }
    int opt = 0;
    char *out = default_out_file;
    char *lst = nullptr;
    while ((opt =  getopt(argc, argv, ":l:o:")) != -1) {
        switch(opt) {
            case 'l':
                lst = optarg;
                break;
            case 'o':
                out = optarg;
                break;
            case ':':
                printf("Option needs a value\n");
                exit(1);
                break;
            case '?':
                printf("Unknown option: %c\n", optopt);
                exit(1);
                break;
            default:
                printf("Unknown option: %c\n", optopt);
                exit(1);
                break;
        }
    }
    if (optind == argc) {
        printf("Filename missing\n");
        exit(1);
    }
    Compiler comp(argv[optind]);
    int prog_size = 0;
    char *text = comp.compile(&prog_size);
    if (lst) {
        FILE *flst = fopen(lst, "w");
        if (flst == NULL) {
            printf("Could not open listing file\n");
            exit(1);
        }
        for (int i = 0; i < comp.lines_count; i++) {
            fprintf(flst,"%#010x    :    %s\n", comp.offset[i], comp.lines[i].ptr);
        }
        fclose(flst);
    }
    FILE *f = fopen(out, "wb");
    if (f == NULL) {
        printf("Could not open output file\n");
        exit(1);
    }
#ifdef INT_OUT
    int cur = *text;
    int c = 0;
    while (c < prog_size) {
        printf("%d ", cur);
        c++;
        cur = *(text + c);
    }
#endif
    fwrite(text, sizeof(char), prog_size, f);
    fclose(f);
    free(text);
    return 0;
}