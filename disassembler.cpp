#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include "haifile.h"
#include "hash_table.hpp"

const char default_keywords_filename[] = "keywords";
char default_out_file[] = "out.s";

class Disassembler {
private:
    struct Command {
        light_string name;
        int argc;
        bool is_label;
        bool is_register;
    };
    Command *commands;
    char *code;
    int* offset;
    int size;
    void load_mapping(const char* keywords_filename) {
        int sz = 0;
        int cnt = 0;
        char *buf = read_file(keywords_filename, &sz);
        light_string* lex = divide_text(buf, sz, &cnt);
        commands = (Command *)calloc(cnt, sizeof(Command));
        for (int i = 0; i < cnt; i++) {
            int tmp = 0, j = 0;
            for (; j < lex[i].length && !isspace(lex[i][j]); j++);
            if (j == 0)
                continue;
            commands[i].name = lex[i];
            commands[i].name.length = j;
            commands[i].argc = lex[i][j + 1] - '0';
            commands[i].is_label = lex[i][j + 3] - '0';
            commands[i].is_register = lex[i][j + 5] - '0';
        }
    }
public:
    int line;
    bool *is_label_needed;
    int* labels;
    Disassembler(const char *text, int sz): line(0){
        code = (char *)calloc(sz, 1);
        memcpy(code, text, sz);
        size = sz;
        is_label_needed = (bool *)calloc(size, sizeof(bool));
        labels = (int *)calloc(size, sizeof(int));
        load_mapping(default_keywords_filename);
    }
    void add_line(light_string str, char ***lines, int *pos, int *max_sz) {
        if (*pos == *max_sz) {
            *max_sz *= 2;
            *lines = (char **)realloc(*lines, sizeof(char *) * *max_sz);
        }
        (*lines)[*pos] = (char *)calloc(256, 1);
        memcpy((*lines)[*pos], str.ptr, str.length);
    }
    char** disassm() {
        int current_position = 0;
        char **listing = (char **)calloc(8, sizeof(char *));
        int max_lines = 8;

        while (current_position < size) {
            int num = code[current_position];
            labels[current_position] = line;
            current_position++;
            add_line(commands[num].name, &listing, &line, &max_lines);
            if (commands[num].argc) {
                int arg = *(int *)(code + current_position);
                int cur_ind = commands[num].name.length;
                listing[line][cur_ind] = ' ';
                cur_ind++;
                current_position += 4;
                if (commands[num].is_label) {
                    listing[line][cur_ind] = 'l';
                    is_label_needed[arg] = true;
                    cur_ind++;
                }
                if (commands[num].is_register) {
                    listing[line][cur_ind] = 'r';
                    cur_ind++;
                }
                sprintf(listing[line] + cur_ind, "%d", arg);
            }
            line += 1;
            if (num == 0)
                break;
        }
        return listing;
    }

};
int main(int argc, char **argv) {
    if (argc == 1) {
        printf("Usage: %s <OPTIONS> <FILE>\n", argv[0]);
        exit(1);
    }
    int opt = 0;
    char *out = default_out_file;
    while ((opt =  getopt(argc, argv, ":o:")) != -1) {
        switch(opt) {
            case 'o':
                out = optarg;
                break;
            case ':':
                printf("Option needs a value\n");
                exit(1);
                break;
            case '?':
                printf("Unknown option: %c\n", optopt);
                exit(1);
                break;
            default:
                printf("Unknown option: %c\n", optopt);
                exit(1);
                break;
        }
    }
    if (optind == argc) {
        printf("Filename missing\n");
        exit(1);
    }
    int size = 0;
    char *buffer = read_file(argv[optind], &size);
    Disassembler dis(buffer, size);
    char **lst = dis.disassm();
    int cur_label = 0;
    int cur_l_pos = 0;
    FILE *fout = fopen(out, "w");
    if (fout == NULL) {
        printf("Output file open error\n");
        exit(1);
    }
    while (cur_l_pos < size && !dis.is_label_needed[cur_l_pos])
        cur_l_pos++;
    cur_label = dis.labels[cur_l_pos];
    for (int i = 0; i < dis.line; i++) {
        if (i == cur_label) {
            fprintf(fout, "l%d:\n", cur_l_pos);
            cur_l_pos++;
            while (cur_l_pos < size && !dis.is_label_needed[cur_l_pos])
                cur_l_pos++;
            if (cur_l_pos != size)
                cur_label = dis.labels[cur_l_pos];
        }
        fprintf(fout, "%s\n", lst[i]);
    }
    fclose(fout);
    return 0;
}
