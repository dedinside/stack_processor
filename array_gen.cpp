//
// Created by dm on 10/21/19.
//
#include "haifile.h"
#include "hash_table.hpp"

const char default_keywords_filename[] = "keywords";
void array_gen(const char* keywords_filename) {
    int sz = 0;
    int cnt = 0;
    char *buf = read_file(keywords_filename, &sz);
    light_string *lex = divide_text(buf, sz, &cnt);

    printf("void (StackProcessor::*_handlers[%d])() = {\n", cnt);
    for (int i = 0; i < cnt; i++) {
        int tmp = 0, j = 0;
        for (; j < lex[i].length && !isspace(lex[i][j]); j++);
        if (j == 0)
            continue;
        printf("    &StackProcessor::");
        for (int it = 0; it < j; it++)
            printf("%c", lex[i][it]);
        printf("_handler,\n");
    }
    printf("};\nfor (int i = 0; i < %d; i++) {\n    handlers[i] = _handlers[i];\n}\n", cnt);
}

int main() {
    array_gen(default_keywords_filename);
}